<?php

namespace Drupal\commerce_multisafepay_recurring\Plugin\Commerce\PaymentMethodType;

use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the Multisafepay payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "multisafepay",
 *   label = @Translation("Multisafepay"),
 * )
 */
class Multisafepay extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@descr' => 'scrap description',
    ];
    return $this->t('Multisafepay (@descr)', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['data'] = BundleFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t('Payment data JSON.'))
      ->setRequired(TRUE);
    
    return $fields;
  }

}