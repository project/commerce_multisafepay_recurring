<?php

namespace Drupal\commerce_multisafepay_recurring\Plugin\Commerce\PaymentType;
use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the default payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_multisafepay",
 *   label = @Translation("Multisafepay"),
 * )
 */
class PaymentMultisafepay extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }
  
}