<p align="center">
  <img src="https://www.multisafepay.com/img/multisafepaylogo.svg" width="400px" position="center">
</p>

# Commerce MultisafePay Recurring

## INTRODUCTION

"Commerce MultisafePay Recurring" module for multisafepay to integrate the MSP recurring payments https://docs.multisafepay.com/api/#recurring-payment.

Version 2 provides full automation of recurring payments in Drupal native way implemented in layers of Commerce core (payments and orders), Commerce Recurring Framework and Commerce MultiSafepay payments.
It add requreid MSP PSP API call ofcource and handles creating and attaching of PaymentMethod to orders, as at the moment related modules not fully ready for Off-site payments without storing payment data inside Drupal.
More about this problem you can ready [here](https://docs.google.com/document/d/14yKyKuAQ-Qkb7xps1pJ8QLLAaA8FSs9ZlomEdRCrJa0/edit?usp=sharing)

## Requirements
- To use the plugin you need a MultiSafepay account. You can create a test account on https://testmerchant.multisafepay.com/signup
- Drupal 8.8.x or 9.x
- Drupal [Commerce](https://www.drupal.org/project/commerce) core 2.x with Payments and Orders enabled
- Drupal [Commerce Recurring Framework](https://www.drupal.org/project/commerce_recurring)
- Drupal [Commerce MultiSafepay payments](https://www.drupal.org/project/commerce_multisafepay_payments) official module from MultiSafepay

## INSTALLATION

Install as you would normally install a Drupal module.

Use `composer require drupal/multisafepayrecurring' to get all dependencies automatically and then enable the "MspRecurring" D9 module.

Ensure to give `Cancel own subscriptions` & `View own subscriptions` permission to authenticated user.

MultiSafepay gateway required HTTPS only.
Ensure your Drupal Url::fromRoute() generates HTTPS links, http -> https redirect will be treated as mistake by MultiSafepay.

## PATCHES
You will need next patches if they are not included into Commerce MultiSafepay payments at the moment:
* [GatewayStandardMethodsHelper::createPayment - wrong syntax](https://www.drupal.org/project/commerce_multisafepay_payments/issues/3224252)
* [Using getOrderNumber() & $order->id() in request](https://www.drupal.org/project/commerce_multisafepay_payments/issues/3224590)
* [Empty profile support](https://www.drupal.org/project/commerce_multisafepay_payments/issues/3221601) (included in master branch already at the moment)
* [Custom PaymentInformation pane support](https://www.drupal.org/project/commerce_multisafepay_payments/issues/3222660)


# About MultiSafepay
MultiSafepay is a collecting payment service provider which means we take care of the agreements, technical details and payment collection required for each payment method. You can start selling online today and manage all your transactions from one place.
## Supported Payment Methods
The supported Payment Methods & Gift cards for this plugin can be found over here: [Payment Methods & Gift cards](https://docs.multisafepay.com/plugins/drupal8/faq/#available-payment-methods-in-drupal8)