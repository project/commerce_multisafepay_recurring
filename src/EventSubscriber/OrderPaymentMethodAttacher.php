<?php
namespace Drupal\commerce_multisafepay_recurring\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

use Drupal\commerce_multisafepay_payments\API\Client;
use Drupal\commerce_multisafepay_payments\Helpers\ApiHelper;
use Drupal\commerce_multisafepay_payments\Helpers\GatewayHelper;
use Drupal\commerce_order\Entity\OrderInterface;

class OrderPaymentMethodAttacher implements EventSubscriberInterface
{
	protected $client;
	protected $mspApiHelper;
	protected $mspGatewayHelper;
	protected $eventSubjectOrder;

	function __construct() {
		$this->client = new Client();
		$this->mspApiHelper = new ApiHelper();
		$this->mspGatewayHelper = new GatewayHelper();
	}

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        $events = [
			// payment_method must be set for creating commerce_subscription, see commerce_recurring/OrderSubscriber
			// lets pray MSP API call for resitering order were done before this event, 
			// otherwise we have no info to create payment method from API request
			'commerce_order.place.pre_transition' => ['attachPaymentMethodByOrderEvent', 100],
            'commerce_payment.authorize_capture.post_transition' => ['attachPaymentMethodByPaymentEvent', -100],
			'commerce_payment.capture.post_transition' => ['attachPaymentMethodByPaymentEvent', -100],
			// RecurringOrderManager::closeOrder() will need payment_method attached to create commerce_payment
			'commerce_order.place.post_transition' => ['attachPaymentMethodFromInitial', -100],
        ];
        return $events;
    }

	protected function saveOrder(OrderInterface $order) {
		// technically commerce_recurring/OrderSubscriber don't create subscription with empty payment_method
		// it just skips orders is payment_method is empty
		// however lets ensure
		$subs = $this->collectSubscriptions($order);
		foreach ($subs as $s) {
			if(empty($s->getPaymentMethod())) {
				$s->setPaymentMethod($order->getPaymentMethod());
				$s->save();
			}
		}

		if($this->eventSubjectOrder !== $order) {
			$res = $order->save();
			// invalidate order cache for Drupal\commerce_payment\PaymentOrderUpdater->updateOrders()
			\Drupal::entityTypeManager()->getStorage('commerce_order')->resetCache([$order->id()]);
			return $res;

		}
		return false;
	}

	  /**
   * {@inheritdoc}
   */
	public function collectSubscriptions(OrderInterface $order) {
    	$subscriptions = [];
    	foreach ($order->getItems() as $order_item) {
			if (!$order_item->hasField('subscription'))
				continue;
      		if ($order_item->get('subscription')->isEmpty()) {
        		// A recurring order item without a subscription ID is malformed.
        		continue;
      		}
      		/** @var \Drupal\commerce_recurring\Entity\SubscriptionInterface $subscription */
      		$subscription = $order_item->get('subscription')->entity;
      		// Guard against deleted subscription entities.
      		if ($subscription) {
        		$subscriptions[$subscription->id()] = $subscription;
      		}
    	}
    	return $subscriptions;
	}

	public function resolvePaymentMethodViaAPI(OrderInterface $order) {
		//get details from MSP API server
		$mode = $this->mspGatewayHelper->getGatewayMode($order);
		$this->mspApiHelper->setApiSettings($this->client, $mode);
		$mspOrder = $this->client->orders->get('orders', commerce_multisafepay_recurring_get_order_id_for_api($order));
		$payment_gateway = $order->payment_gateway->entity;
		//check if already exists ?
		$pm = [
			'type'=>'multisafepay',
			'data'=>json_encode($mspOrder),
			'uid'=>$order->uid->getString(),
			'payment_gateway'=>$payment_gateway->id(),
			'mode' => $mode,
			//'remote_id' => ?
			//'billing_profile' => $payment_method->billing_profile->getString()
		];
		$pm = \Drupal::entityTypeManager()->getStorage('commerce_payment_method')->create($pm);
		$pm->save();
		return $pm;
	}

	public function isNotRecurringOrder(OrderInterface $order, $caller_name='isRecurringOrder', $throw = true) {
		if($order->bundle() != 'recurring') {
			$message = $caller_name . ': Order ' . commerce_multisafepay_recurring_get_order_id_for_api($order) . ' is not a recurring order';
			if($throw) 
				throw new \RuntimeException($message);
			else
				return $message;
		}
		return false;
	}

	public function resolveInitialOrdersFromRecurring(OrderInterface $recurringOrder) {
		$initial_order_ids = [];
		foreach($recurringOrder->getItems() as $order_item) {
			$subscription		= $order_item->get('subscription')->entity;
			$initial_order_id	= $subscription->get('initial_order')->target_id;
			$initial_order_ids[$initial_order_id] = $initial_order_id;
		}
		return $initial_order_ids;
	}

	public function resolveSingleInitialOrdersFromRecurring(OrderInterface $recurringOrder) {
		$initial_order_ids = $this->resolveInitialOrdersFromRecurring($recurringOrder);
		if(count($initial_order_ids) > 1)
			throw new \RuntimeException('attachPaymentMethod: too much initial_order_ids for order ' . commerce_multisafepay_recurring_get_order_id_for_api($recurringOrder));
	
		$initial_order = \Drupal::entityTypeManager()->getStorage('commerce_order')->load(reset($initial_order_ids));
		if(empty($initial_order))
			throw new \RuntimeException('attachPaymentMethod: not able to load initial order by id '.reset($initial_order_ids).' for order ' . commerce_multisafepay_recurring_get_order_id_for_api($recurringOrder));
		return $initial_order;
	}

	public function attachPaymentMethodFromInitial(WorkflowTransitionEvent $event) {
		$order	= $event->getEntity();
		//just skip it, it is normal situation
		if($this->isNotRecurringOrder($order, __FUNCTION__, false)) {
			\Drupal::logger('multisafepay reccuring')->notice('attachPaymentMethodFromInitial not a reccuring ' .commerce_multisafepay_recurring_get_order_id_for_api($order));
			return;
		}
		$this->eventSubjectOrder = $order;
		$result = $this->attachPaymentMethodFromInitialOrder($order);
		$this->eventSubjectOrder = null;
		return $result;
	}

	public function attachPaymentMethodFromInitialOrder(OrderInterface $order) {
		$this->isNotRecurringOrder($order, __FUNCTION__ );
		$initial_order = $this->resolveSingleInitialOrdersFromRecurring($order);
		$payment_method_io = $initial_order->payment_method->entity;
		$payment_gateway = $order->payment_gateway->entity;
		$payment_gateway_io = $initial_order->payment_gateway->entity;

		if(empty($payment_gateway)) {
			if(empty($payment_gateway_io)) 
				throw new \RuntimeException('attachPaymentMethod: payment gateway is empty in initial order '.commerce_multisafepay_recurring_get_order_id_for_api($initial_order).' for order ' . commerce_multisafepay_recurring_get_order_id_for_api($order));
			$order->payment_gateway = $payment_gateway_io;
			$changes[] = 'Payment gateway attached to order ' . commerce_multisafepay_recurring_get_order_id_for_api($order);
		}

		if(!$this->mspGatewayHelper->isMspGateway($payment_gateway_io->getPluginId())) {
			//it is not Multisafepay order
			//if(!empty($changes)) 
			//	\Drupal::logger('multisafepay reccuring')->notice(implode(PHP_EOL, $changes));
			return;
		}

		if(empty($payment_method_io)) {
			//get details from MSP API server
			$pm = $this->resolvePaymentMethodViaAPI($initial_order);
			$payment_method_io = $pm;
			$initial_order->payment_method = $pm;
			$changes_io[] = 'Payment method created for order ' . commerce_multisafepay_recurring_get_order_id_for_api($initial_order);
			$changes_io[] = 'Payment method attached to order ' . commerce_multisafepay_recurring_get_order_id_for_api($initial_order);
		}

		if(empty($payment_method)) {
			$order->payment_method = $initial_order->payment_method->entity;
			$changes[] = 'Payment method attached to order ' . commerce_multisafepay_recurring_get_order_id_for_api($order);
		}
		if(!empty($changes_io)) {
			$this->saveOrder($initial_order);
		}
		if(!empty($changes)) {
			$this->saveOrder($order);
		}

		return true;

	}

	public function attachPaymentMethodByPaymentEvent(WorkflowTransitionEvent $event) {
		//payment also has fileds payment_gateway, payment_gateway_mode (string), payment_method
		$payment	= $event->getEntity();
		$order		= $payment->getOrder();
		$this->eventSubjectOrder = $order;
		$this->attachPaymentMethod($order);
		$this->eventSubjectOrder = null;
	}

	public function attachPaymentMethodByOrderEvent(WorkflowTransitionEvent $event) {
		$order	= $event->getEntity();
		$this->eventSubjectOrder = $order;
		$this->attachPaymentMethod($order);
		$this->eventSubjectOrder = null;

	}


	public function attachPaymentMethod($order) {

		$payment_method		= $order->payment_method->entity;
		$payment_gateway	= $order->payment_gateway->entity;
		$initial_order		= null;

		$changes = [];
		$changes_io = [];

		if(empty($this->isNotRecurringOrder($order, __FUNCTION__, false))) {
			// this case seems not possible, as payment can be created by commerce_reccuring only if payment_method was attached
			// see RecurringOrderManager::closeOrder()
			// but lets keep this code for future or for modules with alternative logic
			$this->attachPaymentMethodFromInitialOrder($order);
		} else {
			$initial_order		= $order;
			$payment_gateway_io	= $payment_gateway;
			$payment_method_io	= $payment_method;
			if(empty($payment_gateway_io))
				throw new \RuntimeException('attachPaymentMethod: payment gateway is empty for order ' . commerce_multisafepay_recurring_get_order_id_for_api($order));

				if(!$this->mspGatewayHelper->isMspGateway($payment_gateway_io->getPluginId())) {
					//it is not Multisafepay order
					$this->eventSubjectOrder = null;
					return;
				}
	
				if(empty($payment_method_io)) {
					//get details from MSP API server
					$pm = $this->resolvePaymentMethodViaAPI($initial_order);
					$payment_method_io = $pm;
					$initial_order->payment_method = $pm;
					$changes_io[] = 'Payment method created for order ' . commerce_multisafepay_recurring_get_order_id_for_api($initial_order);
					$changes_io[] = 'Payment method attached to order ' . commerce_multisafepay_recurring_get_order_id_for_api($initial_order);
				}
				if(!empty($changes_io)) {
					$this->saveOrder($initial_order);
				}
		}
	}
}
